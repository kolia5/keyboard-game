import { renderRoomsPage } from './rooms-page.mjs';

function renderGamePage(room, socket) {
  const gamePage = document.getElementById('game-page');
  const roomsContainer = document.querySelector('#rooms-page');
  const quitRoomBtn = document.getElementById('quit-room-btn');

	roomsContainer.style.display = 'none';
	gamePage.style.display = 'block';

  function onQuitBtn() {
    renderRoomsPage(socket)
  }

  quitRoomBtn.addEventListener('click', onQuitBtn)
}

export {
  renderGamePage
}
