import { showInputModal } from './modal.mjs';

function renderRoomsPage(socket) {
  const gamePage = document.getElementById('game-page');
  const roomsContainer = document.querySelector('#rooms-page');
	roomsContainer.style.display = 'block';
	gamePage.style.display = 'none';

  const createRoomBtn = document.getElementById('add-room-btn');
  let roomName = '';

  function createNewRoom() {
  	socket.emit('CREATE_NEW_ROOM', roomName);
  }

  function changeRoomName(name){
  	roomName = name;
  }

  function onCreateRoom() {
  	showInputModal({
  		title: 'Please enter room name',
  		onSubmit: createNewRoom,
  		onChange: changeRoomName
  	})
  }

  createRoomBtn.addEventListener('click', onCreateRoom)
}

export {
  renderRoomsPage
}
