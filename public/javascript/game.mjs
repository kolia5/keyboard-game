import { showInputModal } from './views/modal.mjs';
import { isUserValid } from './validators/user-validator.mjs';
import { isRoomValid } from './validators/room-validator.mjs';
import { appendRoomElement, clearRoomContainer } from './views/room.mjs';
import { renderRoomsPage } from './views/rooms-page.mjs';
import { renderGamePage } from './views/game-page.mjs';

const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

if(isUserValid){
	renderRoomsPage(socket);
}

function updateAllRooms(rooms) {
	if(rooms.length == 0){
		return null;
	};
	clearRoomContainer();
	rooms.forEach(room => {
		appendRoomElement({
			name: room.name,
			numberOfUsers: room.numberOfUsers,
			onJoin: () => handleOnJoin(room, socket)
		})
	})
}

function handleOnJoin(room, socket) {
	socket.emit('JOIN_ROOM', room.id)
	renderGamePage(room, socket)
}

socket.on('USER_VERIFYING', isUserValid);
socket.on('ROOM_VERIFYING', isRoomValid);
socket.on('UPDATE_ROOMS', updateAllRooms)
socket.on('JOIN_ROOM_DONE', renderGamePage);
