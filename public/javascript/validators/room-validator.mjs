import { showMessageModal } from '../views/modal.mjs';

function isRoomValid(room) {
	if(!room){
		showMessageModal({
			message: 'Room name has already exist'
		})
	}
}

export {
  isRoomValid
}
