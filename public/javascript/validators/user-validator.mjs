import { showMessageModal } from '../views/modal.mjs';

function isUserValid(user) {
	if(!user){
		showMessageModal({
			message: 'User has already exist',
			onClose: () => {
				// if user already exist delete it from sessionStorage and redirect to login
				sessionStorage.removeItem('username');
				window.location.replace('/login');
			}
		})
	}
	return user;
}

export {
  isUserValid
}
