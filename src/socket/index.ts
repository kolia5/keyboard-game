import { Server } from 'socket.io';
import * as config from './config';

import { isUserNameValid } from '../validators/users-validator';
import { isRoomNameValid } from '../validators/rooms-validator';

let users: any[] = [];
let rooms: any[] = [];

const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomId => rooms.find(room => room.id == roomId));

function createNewRoom(roomName, socket){
	if(isRoomNameValid(roomName, rooms)){
		const newRoom = {
			id: socket.id,
			name: roomName,
			numberOfUsers: socket.rooms.size
		};
		rooms = [...rooms, newRoom]
		return newRoom
	}
	return null;
}

function createNewUser(userName, socket) {
	if(isUserNameValid(userName, users)){
		const newUser = {
			id: socket.id,
			name: userName
		};
		users = [...users, newUser]
		return newUser
	}
	return null;
}

export default (io: Server) => {
	io.on('connection', socket => {
		const username = socket.handshake.query.username;
		const newUser = createNewUser(username, socket);

		if(newUser){
			socket.emit('USER_VERIFYING', true)
		} else {
			socket.emit('USER_VERIFYING', false)
		}

		socket.emit('UPDATE_ROOMS', rooms);

		socket.on('CREATE_NEW_ROOM', (roomName) => {
			const newRoom = createNewRoom(roomName, socket);
			if(newRoom){
				socket.emit('JOIN_ROOM_DONE', newRoom);
			} else {
				socket.emit('ROOM_VERIFYING', false);
			}

		})
		socket.on('JOIN_ROOM', (roomId) => {
			// check if socket has been joined to someone room
			const prevRoomId = getCurrentRoomId(socket);
			// if exist, leave this room
			if (prevRoomId) {
        socket.leave(prevRoomId);
      }
			// if that id equals, socket try to join where is it be, just return
      if (roomId === prevRoomId) {
        return;
      }

      socket.join(roomId);
      const room = rooms.find(room => room.id == roomId);
			console.log('rooms.size', socket.rooms.size)
      socket.emit("JOIN_ROOM_DONE", room);

		})
	});


};
