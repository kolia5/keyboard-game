export const isRoomNameValid = (roomName, rooms) => {
  const isAlreadyExist = rooms.find(room => room.name == roomName);
  // if exist, roomName is not valid
  return isAlreadyExist ? false : true
}
